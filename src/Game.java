import java.text.DecimalFormat;

import players.Dealer;
import players.Player;

public class Game {

	public static void main(String[] args) {

		// Create a dealer and a player
		Dealer dealer = new Dealer();
		Player player = new Player();
		
		int winCounter = 0;
		int drawCounter = 0;
		
		// Play 100,000 decks
		for (int i = 0; i < 100000; i++) {
			
			// While this deck still has cards, keep drawing cards
			while(dealer.hasCards()) {
				
				// Increment the total amount of draws
				drawCounter++;
				
				// Give the player 2 cards
				player.addCard( dealer.dealCard() );
				player.addCard( dealer.dealCard() );
				 				
				// If the sum was 21, increment the win-counter
				if (player.getCardSum() == 21) {
					winCounter++;
				}
				
				player.discardCards();
			}
			
			// Create a new deck
			dealer.getNewDeck();
		}
		
		// Calculate and print stuff
		System.out.println("Total games:\t" + drawCounter);
		System.out.println("Total 21s:\t" + winCounter);
		System.out.println();
		System.out.println("Probability to draw 21: " + getPercentage(drawCounter, winCounter) + "%");
	}

	/**
	 * Pretty print a percentage value
	 * 
	 * @param draws - int with the amount of draws 
	 * @param wins - int with the amount of wins
	 * @return - returns a percentage
	 */
	public static String getPercentage(int draws, int wins) {
	    float percentage = ((float) wins) / ((float) draws);
	    percentage = percentage * 100;
	    DecimalFormat f = new DecimalFormat("#.00");
	    return f.format(percentage);
	}	
}


