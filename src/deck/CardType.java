package deck;

/**
 * CardType enum.
 * 
 * Holds all the name and values of the cards.
 */
public enum CardType {
	
	TWO("Two", 2),
	THREE("Three", 3),
	FOUR("Four", 4),
	FIVE("Five", 5),
	SIX("Six", 6),
	SEVEN("Seven", 7),
	EIGHT("Eight", 8),
	NINE("Nine", 9),
	TEN("Ten", 10),
	
	JACK("Jack", 10),
	QUEEN("Queen", 10),
	KING("King", 10),
	
	ACE("Ace", 11);
	
	private int cardValue;
	private String cardName;
	
	private CardType(String name, int value) {
		cardName = name;
		cardValue = value;
	}
	
	public String getName() {
		return cardName;
	}
	
	public int getValue() {
		return cardValue;
	}
}
