package deck;

/**
 * CardSuit enum.
 *
 * Holds the four suits.
 */
public enum CardSuit {

	HEARTS("Hearts"),
	SPADES("Spades"),
	CLUBS("Clubs"),
	DIAMONDS("Diamonds");	
	
	private String cardSuit;
	
	private CardSuit(String cs) {
		cardSuit = cs;
	}
	
	public String getSuit() {
		return cardSuit;
	}
}
