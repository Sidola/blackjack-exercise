package deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Deck {

	private List<Card> deckList = new ArrayList<Card>();
	
	/**
	 * Creates a new deck.
	 */
	public Deck() {
		for (CardSuit suit : CardSuit.values())
			for (CardType type : CardType.values())
				deckList.add( new Card(type, suit) );		
	}
	
	/**
	 * Shuffles the deck.
	 */
	public void shuffleDeck() {
		long seed = System.nanoTime();
		Collections.shuffle(deckList, new Random(seed));		
	}
	
	/**
	 * Checks if the deck still has cards.
	 * 
	 * @return - true if it still has cards, false otherwise
	 */
	public boolean hasCards() {
		return !deckList.isEmpty();
	}
	
	/**
	 * Draws a card from the deck.
	 * 
	 * @return - the card that was drawn
	 */
	public Card drawCard() {
		Card retCard = deckList.get(0);
		deckList.remove(0);
		
		return retCard;
	}
	
	/**
	 * Prints the whole deck.
	 */
	public void printDeck() {
		for (Card card : deckList) {
			System.out.println(card.getSuit() + " - " + card.getName() + " - " + card.getValue());
		}
	}	
}
