package deck;

public class Card {
	private CardSuit cardSuit;
	private CardType cardType;

	/**
	 * Creates a new card.
	 * @param ct - CardType of the card
	 * @param cs - CardSuit of the card
	 */
	protected Card(CardType ct, CardSuit cs) {
		this.cardType = ct;
		this.cardSuit = cs;
	}
	
	/**
	 * Returns the point-value of a card.
	 * 
	 * @return - int with the point-value of the card
	 */
	public int getValue() {
		return cardType.getValue();
	}
	
	/**
	 * Returns the name of the card.
	 * 
	 * @return - String with the name of the card.
	 */
	public String getName() {
		return cardType.getName();
	}
	
	/**
	 * Returns which suit the card belongs to.
	 * 
	 * @return - String with the suit the card belongs to.
	 */
	public String getSuit() {
		return cardSuit.getSuit();
	}
	
}
