package players;

import java.util.ArrayList;
import java.util.List;

import deck.Card;

public class Player {
	
	private List<Card> cards = new ArrayList<Card>();
	
	/**
	 * Returns the sum of the player's cards.
	 *  
	 * @return - int with the sum of the player's cards
	 */
	public int getCardSum() {
		int sum = 0;
		for (Card card : cards) {
			sum += card.getValue();
		}

		return sum;
	}
	
	/**
	 * Adds a card to the player's card list.
	 * 
	 * @param c - Card
	 */
	public void addCard(Card c) {
		cards.add(c);
	}
	
	/**
	 * Discard all the cards in the player's card list.
	 */
	public void discardCards() {
		cards.removeAll(cards);
	}

}
