package players;

import deck.Card;
import deck.Deck;

public class Dealer {

	private Deck deck = null;
	
	/**
	 * Constructor. Creates a new deck.
	 */
	public Dealer() {
		this.getNewDeck();
	}
	
	/**
	 * Creates a new deck and shuffles it.
	 */
	public void getNewDeck() {
		deck = new Deck();
		deck.shuffleDeck();
	}
	
	/**
	 * Deals a card.
	 * 
	 * @return - Card
	 */
	public Card dealCard() {
		return deck.drawCard();
	}
	
	/**
	 * Checks if the dealer still has cards in his deck.
	 * 
	 * @return - true if he still has cards, false otherwise
	 */
	public boolean hasCards() {
		return deck.hasCards();
	}
	
}
